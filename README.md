```
yarn
yarn dev
```

```
yarn
yarn build
```

`.env`

```
VITE_TMDB_API_BASE=https://api.themoviedb.org/3/
```

`.env.local`

```
VITE_BASE_PATH=/
VITE_SUPABASE_ANON_KEY=123...abc
VITE_SUPABASE_URL=https://example.supabase.co
VITE_TMDB_API_KEY=123...abc
```
