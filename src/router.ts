import { createWebHistory, createRouter } from "vue-router";

const history = createWebHistory(import.meta.env.VITE_BASE_PATH);

import FilmDetail from './page/FilmDetail.vue';
import TVDetail from './page/TVDetail.vue';
import WatchList from './page/WatchList.vue';
import DiscoverFilm from './page/DiscoverFilm.vue';
import DiscoverTV from './page/DiscoverTV.vue';
import TVSeason from './page/TVSeason.vue';
import Person from './page/Person.vue';
import Collection from './page/Collection.vue';
import Login from './page/Login.vue';

const routes = [
    { path: '/', component: DiscoverFilm },
    { path: '/tv', component: DiscoverTV },
    { path: '/watch-list', component: WatchList },
    { path: '/film/:filmid', component: FilmDetail },
    { path: '/tv/:tvid', component: TVDetail },
    { path: '/tv/:program_id/season/:season_number', component: TVSeason },
    { path: '/person/:personid', component: Person },
    { path: '/collection/:collectionid', component: Collection },
    { path: '/login', component: Login },
]

const router = createRouter({
    history,
    routes,
});
export default router;
