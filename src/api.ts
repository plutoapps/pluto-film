
const TMDB_API_BASE: string = import.meta.env.VITE_TMDB_API_BASE;
const TMDB_API_KEY: string = import.meta.env.VITE_TMDB_API_KEY;

import { supabase } from "./supabase";

async function discoverMovie(options: any) {
    const params = {
        api_key: TMDB_API_KEY,
        ...options
    };
    const query = new URLSearchParams(params);
    return fetch(`${TMDB_API_BASE}discover/movie?${query.toString()}`)
        .then((response) => response.json());
}

async function discoverTV(options: any) {
    const query = new URLSearchParams({
        api_key: TMDB_API_KEY,
        with_watch_providers: '8',
        ...options
    });
    return fetch(`${TMDB_API_BASE}discover/tv?${query.toString()}`)
        .then((response) => response.json());
}

async function trendingMovie() {
    const query = new URLSearchParams({
        api_key: TMDB_API_KEY
    });
    return fetch(`${TMDB_API_BASE}trending/movie/day?${query.toString()}`)
        .then((response) => response.json())
        .then((data) => data.results);
}

async function searchMovie(searchQuery: string) {
    const query = new URLSearchParams({
        api_key: TMDB_API_KEY,
        query: searchQuery
    });
    return fetch(`${TMDB_API_BASE}search/movie?${query.toString()}`)
        .then((response) => response.json())
        .then((data) => data.results);
}

async function searchAll(searchQuery: string) {
    const query = new URLSearchParams({
        api_key: TMDB_API_KEY,
        query: searchQuery
    });
    return fetch(`${TMDB_API_BASE}search/multi?${query.toString()}`)
        .then((response) => response.json())
        .then((data) => data.results);
}

async function searchKeyword(searchQuery: string) {
    const query = new URLSearchParams({
        api_key: TMDB_API_KEY,
        query: searchQuery
    });
    return fetch(`${TMDB_API_BASE}search/keyword?${query.toString()}`)
        .then((response) => response.json())
        .then((data) => data.results);
}

async function detailMovie(id: string) {
    const query = new URLSearchParams({
        api_key: TMDB_API_KEY,
        append_to_response: 'videos,credits,recommendations,release_dates,watch/providers,keywords'
    });
    const response = await fetch(`${TMDB_API_BASE}movie/${id}?${query.toString()}`)
    const data = await response.json();

    const result = { ...data };

    result.runTime = data.runtime;

    const releaseDateList = data.release_dates?.results;
    const releaseDateLength = releaseDateList?.length;
    let certification = "";
    for (let i = 0; i < releaseDateLength; i++) {
        certification =
            releaseDateList[i]?.release_dates?.[0]?.certification || certification;
        if (releaseDateList[i]?.iso_3166_1 === "GB") {
            break;
        }
    }
    result.certification = certification;

    let director: Array<any> = [];
    let screenplay: Array<any> = [];
    let story: Array<any> = [];
    let producer: Array<any> = [];
    data.credits?.crew?.forEach((crew: any) => {
        switch (crew.job) {
            case "Director":
                director.push(crew);
                break;
            case "Screenplay":
                screenplay.push(crew);
                break;
            case "Story":
                story.push(crew);
                break;
            case "Producer":
                producer.push(crew);
                break;
        }
    });
    result.director = director;
    result.screenplay = screenplay;
    result.story = story;
    result.producer = producer;
    result.release_year = result.release_date?.split("-")?.[0];

    result.videoOption = data.videos?.results
        ?.filter((option: any) => option.site == "YouTube" && option.official)
        ?.sort((a: any, b: any) => a.type != "Trailer")
        ?.map((option: any) => ({ id: option.key, name: option.name }));

    return result;
}

async function wikiMovie(id: any, providers: Array<any>) {
    const result: any = {};
    const sparkQuery = `SELECT%20%3Fitem%20%3FNetflix_ID%20%3Farticle%20WHERE%20%7B%0A%20%20%3Fitem%20wdt%3AP4947%20%22${id}%22.%0A%20%20%3Farticle%20schema%3Aabout%20%3Fitem.%0A%20%20%20%3Farticle%20schema%3AisPartOf%20%3Chttps%3A%2F%2Fen.wikipedia.org%2F%3E.%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP1874%20%3FNetflix_ID.%20%7D%0A%7D`;
    const _wikiData = await wikiData(sparkQuery);
    result.link = _wikiData.results?.bindings?.[0]?.article?.value;
    const netflixId = _wikiData?.results?.bindings?.[0]?.Netflix_ID?.value

    if (netflixId) {
        const netflixData = netflixUrl(netflixId, providers || {});
        result.streamName = netflixData.streamName;
        result.streamUrl = netflixData.streamUrl;
    }
    return result;
}

async function similarMovie(title: string): Promise<any[]> {
    const response = await fetch('https://pluto-film.glitch.me/similar?type=movies&title=' + title);
    const data = await response.json();
    return data.filter((item: null) => item != null);
}

async function similarTV(title: string): Promise<any[]> {
    const response = await fetch('https://pluto-film.glitch.me/similar?type=shows&title=' + title);
    const data = await response.json();
    return data.filter((item: null) => item != null);
}

async function detailTV(id: string) {
    const query = new URLSearchParams({
        api_key: TMDB_API_KEY,
        append_to_response: 'videos,credits,recommendations,watch/providers,external_ids,keywords'
    });
    const response = await fetch(`${TMDB_API_BASE}tv/${id}?${query.toString()}`)
    const data = await response.json();

    const result = { ...data, ...data.external_ids };

    result.runTime = data.episode_run_time?.[0];

    const releaseDateList = data.release_dates?.results;
    const releaseDateLength = releaseDateList?.length;
    let certification = "";
    for (let i = 0; i < releaseDateLength; i++) {
        certification =
            releaseDateList[i]?.release_dates?.[0]?.certification || certification;
        if (releaseDateList[i]?.iso_3166_1 === "GB") {
            break;
        }
    }
    result.certification = certification;

    let director: Array<any> = [];
    let screenplay: Array<any> = [];
    let story: Array<any> = [];
    let producer: Array<any> = [];
    data?.credits?.crew?.forEach((crew: any) => {
        switch (crew.job) {
            case "Director":
                director.push(crew);
                break;
            case "Screenplay":
                screenplay.push(crew);
                break;
            case "Story":
                story.push(crew);
                break;
            case "Producer":
                producer.push(crew);
                break;
        }
    });
    result.director = director;
    result.screenplay = screenplay;
    result.story = story;
    result.producer = producer;
    result.release_date = data.first_air_date?.split("-")?.[0];

    result.videoOption = data.videos?.results
        ?.filter((option: any) => option.site == "YouTube" && option.official)
        ?.sort((a: any, b: any) => a.type != "Trailer")
        ?.map((option: any) => ({ id: option.key, name: option.name }));

    return result;
}

async function wikiTV(id: string, providers: Array<any>) {
    const result: any = {};
    const sparkQuery = `SELECT%20%3Fitem%20%3FNetflix_ID%20%3Farticle%20WHERE%20%7B%0A%20%20%3Fitem%20wdt%3AP4983%20%22${id}%22.%0A%20%20%3Farticle%20schema%3Aabout%20%3Fitem.%0A%20%20%20%3Farticle%20schema%3AisPartOf%20%3Chttps%3A%2F%2Fen.wikipedia.org%2F%3E.%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP1874%20%3FNetflix_ID.%20%7D%0A%7D`;
    const _wikiData = await wikiData(sparkQuery);
    result.wiki = _wikiData.results?.bindings?.[0]?.article?.value;
    const netflixId = _wikiData?.results?.bindings?.[0]?.Netflix_ID?.value

    if (netflixId) {
        const netflixData = netflixUrl(netflixId, providers || {});
        result.streamName = netflixData.streamName;
        result.streamUrl = netflixData.streamUrl;
    }
    return result;
}

function wikiData(sparkQuery: string) {
    return fetch(
        `https://query.wikidata.org/sparql?query=${sparkQuery}&format=json`
    ).then((response) => response.json());
}

function netflixUrl(netflixId: string, providers: any) {
    const available: any = {};
    Object.keys(providers).forEach((p: string) => {
        if (_findNetflixProvider(providers[p].flatrate)) {
            available[p] = true;
        }
    });
    if (netflixId) {
        if (available["GB"]) {
            return {
                streamUrl: "https://www.netflix.com/gb/title/" + netflixId,
                streamName: "Netflix GB"
            }
        } else if (available["US"]) {
            return {
                streamUrl: "https://www.netflix.com/us/title/" + netflixId,
                streamName: "Netflix US"
            }
        }
    }
    return {};
}

function _findNetflixProvider(flatrate: any) {
    let available = false;
    flatrate?.forEach((a: any) => {
        if (a.provider_id === 8) {
            available = true;
        }
    });
    return available;
}

async function fetchYoutubeStream(videoId: string) {
    const response = await fetch(import.meta.env.VITE_VIDEO_API_BASE + "video2?id=" + videoId);
    if (response.status !== 200) {
        return null;
    }
    return response.json();
}

async function collection(collectionId: string) {

    const query = new URLSearchParams({
        api_key: TMDB_API_KEY
    });
    const response = await fetch(`${TMDB_API_BASE}collection/${collectionId}?${query.toString()}`)
    return response.json();
}

async function tvSeason(programId: string, seasonNumber: string) {

    const query = new URLSearchParams({
        api_key: TMDB_API_KEY
    });
    const response = await fetch(
        `${TMDB_API_BASE}tv/${programId}/season/${seasonNumber}?${query.toString()}`)
    return response.json();
}

async function getWatchList(store: any) {
    try {
        if (!store.user) {
            store.watch_list = {};
            return;
        }

        let { data, error, status } = await supabase
            .from("watch_list")
            .select(`media_id, media_type, media_content`)
            .eq("user_id", store.user.id);

        if (error && status !== 406) throw error;

        let updatedWatchList: any = {};
        data?.forEach((row: any) => {
            updatedWatchList[row.media_id] = {
                ...JSON.parse(row.media_content),
                media_type: row.media_type,
            };
        });
        store.watch_list = updatedWatchList;
    } catch (error: any) {
        alert(error.message);
    }
}

async function personDetail(personId: string) {
    const response = await fetch(
        `${import.meta.env.VITE_TMDB_API_BASE}person/${personId}?api_key=${import.meta.env.VITE_TMDB_API_KEY
        }&append_to_response=combined_credits,images,external_ids`
    );
    return response.json()
}

export {
    discoverMovie,
    searchMovie,
    detailMovie,
    trendingMovie,
    searchAll,
    searchKeyword,
    detailTV,
    discoverTV,
    fetchYoutubeStream,
    collection,
    tvSeason,
    getWatchList,
    wikiMovie,
    wikiTV,
    personDetail,
    similarMovie,
    similarTV
};
