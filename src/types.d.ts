declare module 'vue-multiselect';

type ProductionCompany = {
    id: number
    logo_path: string
    name: string
    origin_country: string
}