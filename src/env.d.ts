interface ImportMetaEnv {
    VITE_TMDB_API_BASE: string
    VITE_BASE_PATH: string
    VITE_SUPABASE_ANON_KEY: string
    VITE_SUPABASE_URL: string
    VITE_TMDB_API_KEY: string
}
