import { User } from "@supabase/supabase-js";
import { reactive } from "vue"

export const store = reactive<{
    user: User | null | undefined
    watch_list: any
}>({
    user: null,
    watch_list: {}
});
