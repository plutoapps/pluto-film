export default {
    "movie": [
        {
            "value": "28",
            "text": "Action"
        },
        {
            "value": "12",
            "text": "Adventure"
        },
        {
            "value": "16",
            "text": "Animation"
        },
        {
            "value": "35",
            "text": "Comedy"
        },
        {
            "value": "80",
            "text": "Crime"
        },
        {
            "value": "99",
            "text": "Documentary"
        },
        {
            "value": "18",
            "text": "Drama"
        },
        {
            "value": "10751",
            "text": "Family"
        },
        {
            "value": "14",
            "text": "Fantasy"
        },
        {
            "value": "36",
            "text": "History",
        },
        {
            "value": "27",
            "text": "Horror"
        },
        {
            "value": "10402",
            "text": "Music"
        },
        {
            "value": "9648",
            "text": "Mystery"
        },
        {
            "value": "10749",
            "text": "Romance"
        },
        {
            "value": "878",
            "text": "Science Fiction"
        },
        {
            "value": "10770",
            "text": "TV Movie"
        },
        {
            "value": "53",
            "text": "Thriller"
        },
        {
            "value": "10752",
            "text": "War"
        },
        {
            "value": "37",
            "text": "Western"
        }
    ],
    "tv": [
        {
            "value": 10759,
            "text": "Action & Adventure"
        },
        {
            "value": 16,
            "text": "Animation"
        },
        {
            "value": 35,
            "text": "Comedy"
        },
        {
            "value": 80,
            "text": "Crime"
        },
        {
            "value": 99,
            "text": "Documentary"
        },
        {
            "value": 18,
            "text": "Drama"
        },
        {
            "value": 10751,
            "text": "Family"
        },
        {
            "value": 10762,
            "text": "Kids"
        },
        {
            "value": 9648,
            "text": "Mystery"
        },
        {
            "value": 10763,
            "text": "News"
        },
        {
            "value": 10764,
            "text": "Reality"
        },
        {
            "value": 10765,
            "text": "Sci-Fi & Fantasy"
        },
        {
            "value": 10766,
            "text": "Soap"
        },
        {
            "value": 10767,
            "text": "Talk"
        },
        {
            "value": 10768,
            "text": "War & Politics"
        },
        {
            "value": 37,
            "text": "Western"
        }
    ]
}